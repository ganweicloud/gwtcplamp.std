﻿/*************************************************************************************
 * Copyright (C) 2021 深圳市敢为软件技术有限公司 版权所有。
 * 文 件 名:   Packet.cs
 * 描    述:    
 * 版    本：  V1.0
 * 创 建 者：  沈东
 * 创建时间：  2021-5-20
 * 修 改 者：  沈东
 * 修改时间：  2021-5-21
 * ======================================
*************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWTcpLamp.STD
{
    public class Packet
    {
        byte _head = 0x68;
        byte _end = 0x16;
        byte[] _addr;
        /// <summary>
        /// 控制码 01 读，04 写
        /// </summary>
        byte _ctrl = 0x01;

        byte[] _data;

        public byte Ctrl { get => _ctrl; private set => _ctrl = value; }
        public byte[] Data { get => _data; private set => _data = value; }

        public Packet(byte[] addr, byte ctrl, byte[] data)
        {
            _addr = addr;
            Ctrl = ctrl;
            Data = data;
        }

        public byte[] ToBytes()
        {
            var buffer = new List<byte>();
            buffer.Add(_head);
            buffer.AddRange(_addr);
            buffer.Add(_head);
            buffer.Add(Ctrl);
            buffer.Add((byte)Data.Length);
            var data = Data.ToArray().Select(b => b += 0x33);
            buffer.AddRange(data);
            byte sum = 0;
            foreach (var i in buffer)
            {
                sum += i;
            }
            //check sum
            buffer.Add(sum);
            buffer.Add(_end);
            return buffer.ToArray();
        }

        public static Packet Parse(byte[] buffer)
        {
            var buffer1 = buffer.Skip(2).Take(buffer.Length - 3).ToArray();
            var addr = buffer1.Skip(1).Take(6).ToArray();
            var ctrl = buffer1[8];
            var dataLen = buffer1[9];
            var data = buffer1.Skip(10).Take(dataLen).Select(b => b -= 0x33);
            return new Packet(addr, ctrl, data.ToArray());
        }
    }
}
