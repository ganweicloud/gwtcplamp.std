﻿/*************************************************************************************
 * Copyright (C) 2021 深圳市敢为软件技术有限公司 版权所有。
 * 文 件 名:   CEquip.cs
 * 描    述:    
 * 版    本：  V1.0
 * 创 建 者：  沈东
 * 创建时间：  2021-5-20
 * 修 改 者：  沈东
 * 修改时间：  2021-5-21
 * ======================================
*************************************************************************************/
using GWDataCenter;
using GWDataCenter.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWTcpLamp.STD
{
    public class CEquip : CEquipBase
    {
        Dictionary<ushort, CmdSeg> _cmdSegs = new Dictionary<ushort, CmdSeg>();

        byte[] _addr;

        bool _initFlag = false;

        public override bool init(EquipItem item)
        {

            if (!_initFlag || ResetFlag)
            {
                if (!base.init(item))
                    return false;

                if (!serialport.Initialize(item))
                {
                    return false;
                }
                 
                _addr = item.Equip_addr.Split(' ').Select(b => Convert.ToByte(b, 16)).ToArray();

                _cmdSegs.Clear();
                if (base.ycprows != null)
                {
                    foreach (var dr in ycprows)
                    {
                        var cmd = dr.main_instruction;
                        var dataFlag = Convert.ToUInt16(cmd, 16);

                        if (!_cmdSegs.ContainsKey(dataFlag))
                        {
                            _cmdSegs[dataFlag] = new CmdSeg(_addr, dataFlag);
                        }
                    }
                } 
                if(base.yxprows != null)
                {
                    foreach (var dr in yxprows)
                    {
                        var cmd =dr.main_instruction;
                        var dataFlag = Convert.ToUInt16(cmd, 16);

                        if (!_cmdSegs.ContainsKey(dataFlag))
                        {
                            _cmdSegs[dataFlag] = new CmdSeg(_addr, dataFlag);
                        }
                    }
                } 
                _initFlag = true;
            }

            return true;
        }

        public override CommunicationState GetData(CEquipBase pEquip)
        {
            foreach (var kv in _cmdSegs)
            {
                var cmdSeg = kv.Value;
                var sendBuffer = cmdSeg.Request.ToBytes();
                LogDebug("write data >> " + string.Join(" ", sendBuffer.Select(b => b.ToString("X2"))));
                serialport.Write(sendBuffer, 0, sendBuffer.Length);

                Sleep(serialport.CommWaitTime);
                LogDebug($"sleep time {serialport.CommWaitTime}");

                var buffer = new byte[256];
                int len = serialport.Read(buffer, 0, 256);
                if (len < 10)
                {
                    return CommunicationState.fail;
                }
                LogDebug("read data >> " + string.Join(" ", buffer.Take(len).Select(b => b.ToString("X2")))); 
                cmdSeg.Response = Packet.Parse(buffer.Take(len).ToArray());
            }

            return base.GetData(pEquip);
        }

        public override bool GetYC(YcpTableRow r)
        {
            var cmd = r.main_instruction.ToString();
            var dataFlag = Convert.ToUInt16(cmd, 16);

            if (!_cmdSegs.ContainsKey(dataFlag))
            {
                LogDebug("找不到命令参数:" + cmd);
                return false;
            }
            var cmdSeg = _cmdSegs[dataFlag];
            var cmdParm = r.minor_instruction.ToString();
            var cmdParms = cmdParm.Split(',');
            var index = Convert.ToInt32(cmdParms[0]);
            var len = Convert.ToInt32(cmdParms[1]);
            var type = cmdParms[2];

            if (cmdSeg.Response.Data.Length == 1)
            {
                LogDebug("写入失败，远程主机返回错误:" + cmdSeg.Response.Data[0].ToString("X2"));
                return false;
            }

            var data = cmdSeg.Response.Data.Skip(2);
            if (index + len > data.Count())
            {
                LogDebug($"获取数据不正确:位置{index},长度{len}，命令参数：{cmdParm}");
                return false;
            }
            var valueBytes = data.Skip(index).Take(len).ToArray();
            switch (type)
            {
                case "i":
                    {
                        var intBytes = new byte[4];
                        Array.Copy(valueBytes, intBytes, len);
                        var value = BitConverter.ToInt32(intBytes, 0);
                        SetYCData(r, value);
                        return true;
                    }
                case "f":
                    {
                        var floatBytes = new byte[4];
                        Array.Copy(valueBytes, floatBytes, len);
                        var value = BitConverter.ToSingle(floatBytes, 0);
                        SetYCData(r, value);
                        return true;
                    }
                default:
                    LogDebug($"数据转换类型不正确，不存在转换类型：{type}");
                    break;
            }


            return false;
        }

        public override bool GetYX(YxpTableRow r)
        {
            var cmd = r.main_instruction.ToString();
            var dataFlag = Convert.ToUInt16(cmd, 16);

            if (!_cmdSegs.ContainsKey(dataFlag))
            {
                LogDebug("找不到命令参数:" + cmd);
                return false;
            }
            var cmdSeg = _cmdSegs[dataFlag];
            var cmdParm = r.minor_instruction.ToString();  
            var cmdParms = cmdParm.Split('.');
            var index = Convert.ToInt32(cmdParms[0]);
            var bit = Convert.ToInt32(cmdParms[1]);

            if(cmdSeg.Response.Data.Length == 1)
            {
                LogDebug("写入失败，远程主机返回错误:" + cmdSeg.Response.Data[0].ToString("X2"));
                return false;
            }

            var data = cmdSeg.Response.Data.Skip(2);
            if (index >= data.Count())
            {
                LogDebug($"获取数据不正确,命令参数：{cmdParm}");
                return false;
            }
            var valueByte = data.ToArray()[index];
            var bits = new BitArray(new byte[] { valueByte });
            if (bit >= bits.Length)
            {
                LogDebug($"超出位索引：{bit}");
                return false;
            }
            var value = bits[bit];
            SetYXData(r, value);
            return true;
        }

        public override bool SetParm(string MainInstruct, string MinorInstruct, string Value)
        {
            byte[] values;
            switch (MinorInstruct)
            {
                case "light": values = new byte[] { Convert.ToByte(Value), Convert.ToByte(Value) }; break;
                default: values = Value.Split(' ').Select(s => Convert.ToByte(s, 16)).ToArray(); break;
            }
            var dataFlag = Convert.ToUInt16(MainInstruct, 16);
            var data = new List<byte>();
            data.Add((byte)(dataFlag % 256));
            data.Add((byte)(dataFlag / 256));
            data.AddRange(values);
            var request = new Packet(_addr, 04, data.ToArray());
            var writeBuffer = request.ToBytes();

            LogDebug("write data >> " + string.Join(" ", writeBuffer.Select(b => b.ToString("X2"))));

            serialport.Write(writeBuffer, 0, writeBuffer.Length);

            System.Threading.Thread.Sleep(serialport.CommWaitTime);

            var readBuffer = new byte[256];
            var len = serialport.Read(readBuffer, 0, readBuffer.Length);
            LogDebug("read data >> " + string.Join(" ", readBuffer.Take(len).Select(b => b.ToString("X2"))));

            if (len < 10)
            {
                LogDebug("写入失败，远程主机返回错误:" + len);
                return false;
            }

            var respone = Packet.Parse(readBuffer);
            if (respone.Data.Length == 1)
                return false;
            return true;
        }

        public void LogDebug(string msg)
        {
            MessageService.AddMessage(MessageLevel.Debug, msg, m_equip_no);
        }
    }
}
