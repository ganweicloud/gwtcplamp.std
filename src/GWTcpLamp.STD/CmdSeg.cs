﻿/*************************************************************************************
 * Copyright (C) 2021 深圳市敢为软件技术有限公司 版权所有。
 * 文 件 名:   CmdSeg.cs
 * 描    述:    
 * 版    本：  V1.0
 * 创 建 者：  沈东
 * 创建时间：  2021-5-20
 * 修 改 者：  沈东
 * 修改时间：  2021-5-21
 * ======================================
*************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWTcpLamp.STD
{
    public class CmdSeg
    {
        Packet request;

        Packet response;

        public Packet Request { get => request; set => request = value; }
        public Packet Response { get => response; set => response = value; }

        public ushort DataFlag { get; }

        public CmdSeg(byte[] addr, ushort dataFlag)
        {
            DataFlag = dataFlag;
            request = new Packet(addr, 01, new byte[] { (byte)(dataFlag % 256), (byte)(dataFlag / 256) });
        }
    }
}
