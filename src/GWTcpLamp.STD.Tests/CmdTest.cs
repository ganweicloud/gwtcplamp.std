﻿/*************************************************************************************
 * Copyright (C) 2021 深圳市敢为软件技术有限公司 版权所有。
 * 文 件 名:   CmdTest.cs
 * 描    述:    
 * 版    本：  V1.0
 * 创 建 者：  沈东
 * 创建时间：  2021-5-20
 * 修 改 者：  沈东
 * 修改时间：  2021-5-21
 * ======================================
*************************************************************************************/
using System;
using System.Linq;
using GWTcpLamp.STD;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GWLampR5F.NET.Tests
{
    [TestClass]
    public class CmdTest
    {
        [TestMethod]
        public void 操作参数输入测试()
        {
            var cmd = "CF02";
            var dataFlag = Convert.ToUInt16(cmd, 16);
            CmdSeg cmdSeg = new CmdSeg(new byte[] { 1, 0, 0, 0, 0, 0 }, dataFlag);
            var data = cmdSeg.Request.ToBytes();
            Console.WriteLine(string.Join(" ", data.Select(d => d.ToString("X2"))));
            Assert.IsTrue(data[9] == 2);
            Assert.IsTrue(data[10] == 0x35);
            Assert.IsTrue(data[11] == 0x02);
        }

        [TestMethod]
        public void 接收字节转换为封包测试()
        {
            var data = new byte[] { 0xFE, 0xFE, 0x68, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x68, 0x04, 0x09, 0x34, 0xE4, 0x53, 0x37, 0x39, 0xDD, 0xDD, 0x33, 0x33, 0x6E, 0x16, 0x00 };
            var packet = Packet.Parse(data);

            Assert.IsTrue(packet.Ctrl == 4);
            Assert.IsTrue(packet.Data.Length == 9);
            Assert.IsTrue(packet.Data[0] == 0x01);
        }

        [TestMethod]
        public void 亮度值测试()
        {
            var data = new byte[] { 0xFE, 0xFE, 0x68, 0x73, 0x87, 0x27, 0x00, 0x00, 0x00, 0x68, 0x81, 0x12, 0x34, 0x02, 0x33, 0x3B, 0x33, 0x33, 0x33, 0x33, 0x6B, 0x33, 0x75, 0x3C, 0x13, 0x37, 0x33, 0x33, 0x97, 0x33, 0xBD, 0x16, 0x00 };
            var packet = Packet.Parse(data);

            var len = 2;
            var index = 14;
            var valueBytes = packet.Data.Skip(2).Skip(index).Take(len).ToArray();
            var intBytes = new byte[4];
            //Array.Reverse(valueBytes);
            Array.Copy(valueBytes, intBytes, len);
            var value = BitConverter.ToInt32(intBytes, 0);

            Assert.IsTrue(value == 100);
        }
    }
}
